//
//  peopleCell.h
//  sampleAdressBook
//
//  Created by bytedance on 2021/9/24.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "peopleInfo.h"
NS_ASSUME_NONNULL_BEGIN

@interface peopleCell : UITableViewCell
@property(nonatomic,strong) UILabel* headPhoto;
@property(nonatomic,strong) UILabel* name;
@property(nonatomic,strong) UILabel* signature;
@property(nonatomic,strong) UILabel* relation;
@property(nonatomic,strong) peopleInfo* people;
-(void)setPeopleInfo:(peopleInfo*)people;
@end


NS_ASSUME_NONNULL_END
