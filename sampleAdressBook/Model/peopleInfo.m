//
//  peopleInfo.m
//  sampleAdressBook
//
//  Created by bytedance on 2021/9/24.
//

#import "peopleInfo.h"
/*
 Note:
    peopleInfo
    1.name -> 姓名
    2.headPhoto -> 头像
    3.signature -> 个性签名
    4.relation -> 关系
 */
@implementation peopleInfo
-(instancetype)initWithFrame:(NSString*)name
                            andHeadPhoto :(NSString*)headPhoto
                            andSignature :(NSString*)signature
                            andRelation :(NSString*)relation
{
    
    if(self = [super init]){
        self.headPhoto = headPhoto;
        self.name = name;
        self.relation = relation;
        self.signature = signature;
    }
    return self;
}
@end
