//
//  peopleCell.m
//  sampleAdressBook
//
//  Created by bytedance on 2021/9/24.
//

#import "peopleCell.h"
#import "peopleInfo.h"
/*
 Note:
    peopleInfo
    1.name -> 姓名
    2.headPhoto -> 头像
    3.signature -> 个性签名
    4.relation -> 关系
    cell heigth -> 40.0f
 */
@implementation peopleCell
//对initWithStyle进行重写
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier: (NSString *)reuseIdentifier{
    
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        if([reuseIdentifier isEqual:@"cellid"]){
            //1.组件初始化
            self.name = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, 80, 25)];
            self.relation = [[UILabel alloc]initWithFrame:CGRectMake(140,0, 50, 25)];
            self.signature = [[UILabel alloc]initWithFrame:CGRectMake(60,25,200, 25)];
            self.headPhoto = [[UILabel alloc]initWithFrame:CGRectMake(0,0, 50, 50)];
            
            [self.contentView addSubview:_name];
            [self.contentView addSubview:_relation];
            [self.contentView addSubview:_signature];
            [self.contentView addSubview:_headPhoto];
        }
    }
    return self;
}
-(void)setPeopleInfo:(peopleInfo*) people{
    self.headPhoto.textAlignment = NSTextAlignmentCenter;
    self.headPhoto.text = people.headPhoto;
    
    self.name.textAlignment = NSTextAlignmentCenter;
    self.name.text = people.name;
    self.name.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    self.name.baselineAdjustment = UIBaselineAdjustmentNone;
    
    self.relation.textAlignment = NSTextAlignmentLeft;
    self.relation.text = people.relation;
    self.relation.baselineAdjustment = UIBaselineAdjustmentNone;
    self.relation.font = [UIFont fontWithName:@"Helvetica" size:10];
    self.relation.highlighted = YES;
    self.relation.highlightedTextColor = [UIColor blackColor];
    self.relation.shadowColor = [UIColor systemBlueColor];
    self.relation.shadowOffset = CGSizeMake(-0.5, 0.5);
 
    self.signature.textAlignment = NSTextAlignmentLeft;
    self.signature.font = [UIFont fontWithName:@"Helvetica" size:13];
    self.signature.textColor =  [UIColor systemGrayColor];
    self.signature.lineBreakMode = NSLineBreakByTruncatingTail; // 截取后面文字 .. 显示
    self.signature.text = people.signature;
}
@end
