//
//  peopleInfo.h
//  sampleAdressBook
//
//  Created by bytedance on 2021/9/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface peopleInfo : NSObject

@property(nonatomic,strong) NSString* name;
@property(nonatomic,strong) NSString* headPhoto;
@property(nonatomic,strong) NSString* signature;
@property(nonatomic,strong) NSString* relation;
-(instancetype)initWithFrame:(NSString*)name
                            andHeadPhoto :(NSString*)headPhoto
                            andSignature :(NSString*)signature
                            andRelation :(NSString*)relation;
@end

NS_ASSUME_NONNULL_END
