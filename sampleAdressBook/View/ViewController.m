//
//  ViewController.m
//  sampleAdressBook
//
//  Created by bytedance on 2021/9/24.
//

#import "ViewController.h"
#import "peopleCell.h"
#import "peopleInfo.h"

@interface ViewController ()
<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong) NSMutableArray<peopleInfo*>* peopleArray;
@property(nonatomic,strong) UITableView* myTable;
@property(nonatomic,strong) UILabel* header;
@property(nonatomic,strong) NSMutableArray* sectionModel;
@end

@implementation ViewController
#pragma mark 各个属性
/*
    myTable -> 通讯录表格
    header -> 表格
    peopleArray -> 储存通讯录信息
    sectionModel -> 每个分快信息
*/
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initData];
}

-(void)initData{
   /*
        1. 初始化UITableView
        2. 设置代理
        3. 是否进行注册(关联)
   */
    
    //自定义数据
    self.peopleArray = [[NSMutableArray alloc]initWithObjects:
                        [[peopleInfo alloc]initWithFrame:@"谢文光" andHeadPhoto:@"头像" andSignature:@"一点浩然气,千里快哉风" andRelation:@"朋友"],
                        [[peopleInfo alloc]initWithFrame:@"孙继新" andHeadPhoto:@"头像" andSignature:@"当你在凝望深渊时,深渊也在凝望你." andRelation:@"家人"],
                        [[peopleInfo alloc]initWithFrame:@"张清林" andHeadPhoto:@"头像" andSignature:@"锄禾日当午,汗滴禾下土.谁知盘中餐,粒粒皆辛苦." andRelation:@"朋友"],
                        [[peopleInfo alloc]initWithFrame:@"阿西吧" andHeadPhoto:@"头像" andSignature:@"一万年太久,只争朝夕." andRelation:@"死党"],
                        [[peopleInfo alloc]initWithFrame:@"阿尤根" andHeadPhoto:@"头像" andSignature:@"啊吧吧吧啊吧吧吧吧吧吧啊吧吧吧吧吧" andRelation:@"朋友"],
                        [[peopleInfo alloc]initWithFrame:@"铁山靠" andHeadPhoto:@"头像" andSignature:@"滨州网红" andRelation:@"朋友"],
                        nil];

    UILocalizedIndexedCollation* collation = [UILocalizedIndexedCollation currentCollation];
    NSInteger sectionCount = [[collation sectionTitles]count];
    //初始化sectionModel
    self.sectionModel = [[NSMutableArray alloc]initWithCapacity:sectionCount];
    for(NSInteger i=0;i<sectionCount;i++)
        [self.sectionModel addObject:[[NSMutableArray alloc]initWithCapacity:50]];
    for(peopleInfo* obj in self.peopleArray){
        NSInteger sectionNumber = [collation sectionForObject:obj collationStringSelector:@selector(name)];
        NSLog(@"%@->%lu",obj.name,sectionNumber);
        [[self.sectionModel objectAtIndex:sectionNumber]addObject:obj];
    }
    
    //初始化
    self.header = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*0.2)];
    self.header.text = @"简易版通讯录";
    self.header.font = [UIFont fontWithName:@"Copperplate-Bold" size:25];
    self.header.textAlignment = NSTextAlignmentCenter;
    self.header.baselineAdjustment = UIBaselineAdjustmentNone;
    self.header.backgroundColor = [UIColor secondarySystemFillColor];
    [self.view addSubview:self.header];
    
    self.myTable = [[UITableView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height*0.2, self.view.frame.size.width, self.view.frame.size.height*0.8) style:UITableViewStylePlain];
    [self.view addSubview:self.myTable];
    
    //设置代理
    self.myTable.delegate = self;
    self.myTable.dataSource = self;
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //自定义tableViewCell
    peopleCell* cell;
    static NSString *identifier = @"cellid";
    /*
        非注册方式复用 需要判空
     */
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell== nil)
        cell = [[peopleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellid"];
    [cell setPeopleInfo:[[self.sectionModel objectAtIndex:indexPath.section]objectAtIndex:indexPath.row]];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
    
}

// rows in section
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [[self.sectionModel objectAtIndex:section]count];
}
// sections in table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.sectionModel count];
}
//height
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}
//section title
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSUInteger scot = [[self.sectionModel objectAtIndex:section]count];
    NSLog(@"%lu",scot);
    if(scot == 0) return nil;
    if(section == 27) return @"#";
    return [[NSString alloc]initWithFormat:@"%c",(int)(section+'A')];
}
@end
