//
//  SceneDelegate.h
//  sampleAdressBook
//
//  Created by bytedance on 2021/9/24.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

